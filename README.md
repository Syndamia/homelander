# Homelander <img src="./img/logo.png" height="29" width="29">

A tiny, good looking and simple zsh prompt.
The goal is to simplify making your prompt, while also being as small as possible.

`homelander.zsh` has the main logic, powering the theme, in 200 lines or less (with documentation), or your money back!  
`homelander-sections.zsh` has prebuilt sections, you can not use it if you want.

### Contents

1. [Configuring](#configuring)
   1. [Colors](#colors)
   2. [Sections](#sections)
   3. [Asynchronicity](#asynchronicity)
2. [Installation](#installation)
   1. [Manual](#manual)
3. [Screenshots](#screenshots)

## Configuring

Configuring your prompt is very much done the manual way, but homelander simplifies this process significantly.

The prompt to the left of your cursor is specified by the variable `PROMPT`, and the one on the right (edge of the screen) is specified by `RPROMPT`.
Those variables are just specifically formatted character strings, surrounded in **single quotes** (`'`).
Multiple lines are done by putting a `\n` somewhere in `PROMPT` (or `RPROMPT`), all stuff afterwards are on a new line.

Everything that isn't just static text, like the amount of background tasks or the current time, needs to be "generated" by a function.
You call a function in `PROMPT` (or `RPROMPT`) by adding

```
$(functionName parameter1 parameter2 ...)
```

### Colors

Quite often you might want to add color to your text and surround it with some characters, like this:

![](./img/username-pill.png)

Or better yet, you might want to have multiple stuff next to each other, with separators and different colors:

![](./img/multiple-pill.png)

Doing this manually is a hassle, so homelander provides 3 functions that fill most needs (more detail in [homelander.zsh](./homelander.zsh)):

- [`hl_cecho`](https://gitlab.com/Syndamia/homelander/-/blob/main/homelander.zsh#L89) takes three argument, foreground color code, background color code and a string.
  The string has those exact colors set.  

  Bad example:
  ```
  hl_cecho 223 66 kamen
  ```
  produces ![](./img/username-colored.png)

- [`hl_cecho_caps`](https://gitlab.com/Syndamia/homelander/-/blob/main/homelander.zsh#L101) same as `hl_cecho`, however it allows for two characters at the ends (that I call caps),
  which will be colored with swapped colors, effectively making a shape.  

  Bad example:
  ```
  hl_cecho_caps 223 66 kamen  
  ```
  produces ![](./img/username-pill.png)

- [`hl_concatsec`](https://gitlab.com/Syndamia/homelander/-/blob/main/homelander.zsh#L150) creates that line with multiple differently colored parts.
  It takes information about the end caps, separator and then every part is in the same format,
  foreground color, background color, text.
  The only caveat is that if you want a separator to be the empty string, you'll have to use `\b`.  

  Bad example:
  ```
  hl_concatsec \b  \b  \b  0   223 66 kamen   214 0 "12:54"   223 172 
  ```
  produces ![](./img/multiple-pill.png)

### Sections

However, we still need to generate some of those stuff too!
The username should change, depending on the logged in user, the time needs to change and so on.

Each such piece of information is separated into it's own "generator" function, which will "return" (`echo -en`) the foreground color, background color and text, all ready to use.
I call such functions (and all of the variables and helper functions they use) "sections".

A proper example of how we would use `hl_concatsec` is:

```
hl_concatsec \b  \b  \b  0   $(hl_user) $(hl_time) $(hl_distro_icon)
```

It's important to note that we **mustn't** put quotes around the "inner" function calls!
A wide variety of pre made sections exist in `homelander-sections.zsh`

Homelander also provides the `lc_mode` function, which will return a successful exit code, when the terminal is running with a small amount of colors (like in a tty).
On all pre made sections, variable values for colors and characters are chosen depending on that, so your setup will look fine even on such terminals.

### Asynchronicity

The final problem we have is that of sections like the clock, which need to be updated all the time, and git information sections, since git can take a while to show the data and nobody wants to wait for their theme.

Homelander provides 2 functions for that purpose:

- [`hl_run_async`](https://gitlab.com/Syndamia/homelander/-/blob/main/homelander.zsh#L116) which will run a function (without parameters) in the background and output it's result in a temporary file
- [`hl_run_async_cont`](https://gitlab.com/Syndamia/homelander/-/blob/main/homelander.zsh#L133) which endlessly reruns a function by some given period, and pass it the last result

However, those functions aren't used in `PROMPT`, they're used in a function called `hl_hooks`.
This function should be filled in your own `.zshrc`.

Finally, since it might not be obvious how is best for a section to be ran in the background, all pre made sections in `homelander-sections.zsh` that require background operation have a `functionName_async` function, which sets that up and doesn't require any arguments.

All in all, for our clock to work, all we need is:

```
hl_hooks() {
    hl_time_async
}
```

### Putting it all together

The configuration in your `.zshrc` (without the sourcing of homelander):

```
export PROMPT='$(hl_concatsec \b  \b  \b  0 $(hl_user) $(hl_time) $(hl_distro_icon)) $(hl_cecho $(hl_precursor)) '
export RPROMPT=''

hl_hooks() {
    hl_time_async
    hl_precursor_async
}
```

produces:

![](./img/leftprompt-finished.png)

Where `hl_precursor` is shown as a double arrow (as in the screenshot) normally, and shows a specific character when in a git repository.

## Installation

### Manual

1. Download the `homelander.zsh` file
```bash
curl https://gitlab.com/Syndamia/homelander/-/raw/main/homelander.zsh -o homelander.zsh
```

2. Source it in your `.zshrc` or in a running shell (remember to change the path if you downloaded it elsewhere)
```bash
source ~/homelander.zsh
```

**IMPORTANT:** If you want to also use the pre made sections, you'll have to do both steps for `homelander-sections.zsh`.  

Alternatively

1. Clone the repository
```bash
git clone https://gitlab.com/Syndamia/homelander.git
```

2. Source the files you want in your `.zshrc`

## Screenshots

![](./img/screenshot.png)

*Normal mode with Terminator as terminal emulator and MesloLGS NF font*

![](./img/screenshot-lc.png)

*Low colour mode in a virtual terminal with font Lat15-Fixed16.psf.gz (default on Debian)*
