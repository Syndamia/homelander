##################################################
# Author  : Syndamia <kamen@syndamia.com>
# Version : 
# Source  : https://gitlab.com/Syndamia/homelander
##################################################
#
# This file contains the "back-end" configurations, as well as
# a lot of helper functionalities.
# Some predefined sections, are available in homelander-sections.zsh
#
# You're pretty much required to write your own config, by
# at least changing PROMPT and RPROMPT variables.
# In them, normal text will be shown as-is and stuff like:
# $(function parameter1 parameter2 ...)
# will call functions (that are made to "generate" text).
#
# I call a section any text (colored or not) which shows a single
# piece of information, like your username or the time.
# There are 3 function to help you format sections (text):
# - hl_cecho, prints colored text
# - hl_cecho_caps, prints colored text which has characters
#                  (which I call caps) at both ends with reverse colors
# - hl_concatsec, prints multiple differently colored texts with
#                 properly colored separators (and end caps)
# If the text takes a lot of time to generate, or it needs to update,
# you can run it in the background with
# - hl_run_async, the function is ran in the background and text is shown
#                 when it's finished
# - hl_run_async_cont, where the text is redrawn on intervals
# All of those are explained in more detail at the bottom of the file.
#
# For more information, read the README.md
#
# #################################################

  # If terminal doesn't support at least 256 colours, we go into low colour mode.
  # For each set of variables that start with "lc_mode", the first variable
  # column is the fallback values, while the second (last) column holds the normal values
  # Also I assume if a terminal supports less than 256 colors, it won't support
  # fancy characters (like those in NERDfonts).
[ "$(tput colors)" -lt 256 ]; _lc=$?
lc_mode() { return $_lc ; }

lc_mode && HL_PROMPT_FG=7 || HL_PROMPT_FG=223
lc_mode && HL_PROMPT_BG=0 || HL_PROMPT_BG=0

  # All async commands must be declared in a function with this name.
  # They will be started before the prompt is drawn.
hl_hooks() {}

export PROMPT='$(hl_concatsec \b  \b  \b  0 $(hl_user) $(hl_time) $(hl_distro_icon)) $(hl_cecho $(hl_precursor)) ' # Left prompt
export RPROMPT=''   # Right prompt

# == Prompt hooks ==

chpwd() {
	_print_directory=1
}

precmd() {
	export _exit_code="$?"

    # Negative PID kills itself and subprocesses
	[ -n "$_hooks_par" ] && kill -SIGTERM -- -$_hooks_par >/dev/null 2>&1
	# forked (disowned) process, that contains all hooks (subproceses)
	(hl_hooks; wait) &!
	_hooks_par=$!

	[ -n "$_exec_start" ] && _exec_dur="$(( $(date +%s) - _exec_start ))"
	_exec_start=''

	export _jobs="$(jobs | wc -l)"
}

preexec() {
	_exec_start="$(date +%s)"
	_print_directory=0
}

TRAPUSR1() {
	zle && zle reset-prompt
}

trap 'kill -SIGTERM -- -$_hooks_par' EXIT # because TRAPEXIT is either broken or works differently
setopt prompt_subst # required, so the functions are called each time the prompt has to be redrawn

# == Helper and print functions ==

    # $1  foreground colour  [REQUIRED]
    # $2  background colour  [REQUIRED]
    # $3  text to print      [REQUIRED]
    #
    # Entering "o" for $1 and $2 uses the default PROMPT colours.
hl_cecho() {
	[ "$1" = 'o' ] && 1=$HL_PROMPT_FG
	[ "$2" = 'o' ] && 2=$HL_PROMPT_BG

	echo -en "%F{$1}%K{$2}$3%k%f"
}

    # $1  text foreground colour  [OPTIONAL]
    # $2  text background colour  [OPTIONAL]
    # $3  text to print           [OPTIONAL]
    # $4  left cap text           [OPTIONAL]
    # $5  right cap text          [OPTIONAL]
    #
    # Prints text, with two "caps" at the sides, whose foreground colour is the
    # text's background colour (and whose background colour is PROMPT's).
    # Entering "o" for $1 and $2 uses the default PROMPT colours.
hl_cecho_caps() {
	[ -n "$4" ] && hl_cecho "$2" "$HL_PROMPT_BG" "$4"
	[ -n "$3" ] && hl_cecho "$1" "$2"            "$3"
	[ -n "$5" ] && hl_cecho "$2" "$HL_PROMPT_BG" "$5"
}

    # $1  function to be ran async        [REQUIRED]
    # $2  default value in funciton file  [OPTIONAL]
    # 
    # The function's output is stored in a file, named /tmp/hl_async_$1 (where $1 is
    # the funciton name). The file contents aren't changed when function outputs nothing.
    # If the function's exit code is zero, PROMPT (and RPROMPT) is redrawn.
    #
    # Big thanks to https://www.anishathalye.com/2015/02/07/an-asynchronous-shell-prompt/
hl_run_async() {
	(
		[ -n "$2" ] && echo "$2" > "/tmp/hl_async_$1"
		local __out="$($1)"
		[ -n "$__out" ] && echo "$__out" > "/tmp/hl_async_$1"
		[ "$?" -eq 0 ] && kill -USR1 $$
	) &
}

    # $1  function to be ran async              [REQUIRED]
    # $2  time to sleep between function calls  [REQUIRED]
    #
    # The function is ran in an endless loop with $2 time between calls.
    # The function output is stored in a variable that is passed to the function.
    # Exit code of zero redraws PROMPT (and RPROMPT).
hl_run_async_cont() {
	(
		local __var=''
		while true; do
			__var="$($1 $__var)"
			[ "$?" -eq 0 ] && kill -USR1 $$
			sleep $2
		done
	) &
}

    # Creates a colored line with proper handling of (colors of) separators and caps
    # The first 5 parameters specify the caps and separators, while every following
    # 3 values specify each information entry.
    #
    # First 5 are ordered as (where left & right are end cap characters, sep is separator character):
    # left right sep sep-type
    #
    # Every consequtive 5 values are in the form (fg & bg are colors):
    # fg   bg   text
    #
    # Left and right caps colors are set according to the first and last entry.
    # Colors are numbers or "o" (use default prompt colors).
    # Caps and separator are text (preferably a single character) or '\b' when they shouldn't exist.
    # sep-type specifies how the separator colors are handled,
    #   0 means it is treated as a filled shape ( or ), and
    #     fg is the color of the left entry, while bg is the color of the right entry
    #   1 means it is treated as a thin shape ( or ), where
    #     fg and bg are those of the first entry
    #
    # Example:
    # hl_concatsec '\b'  ''  '\b'  ''  '\b'  ''  0  \
    #              o     66   " $USER "      \
    #              o     160  " $(pwd) "
    # Will create:
    #  john  /home/john 
    # where " john " will have a blue background,
    #       " /home/john " will have a red background,
    #       and all separators will properly show color "transitions"
    #       (i.e. foreground and background will match a separator or cap surroundings)
hl_concatsec() {
	# Left cap
	[ $2 != '\b' ] && hl_cecho "${@[9]}" "$HL_PROMPT_BG" "$2"
	
	for (( i=8 ; i < # ; i += 3 )); do
		# Separator
		if [ $6 != '\b' -a $i -gt 8 ]; then
			if [ $7 -eq 0 ]; then
				hl_cecho "${@[$((i - 2))]}" "${@[$((i + 1))]}" "$6"
			else
				hl_cecho "${@[8]}" "${@[9]}" "$6"
			fi
		fi
		# Middle text
		hl_cecho "${@[i]}" "${@[$((i + 1))]}" "${@[$((i + 2))]}"
	done

	# Right cap
	[ $4 != '\b' ] && hl_cecho "${@[$((# - 1))]}" "$HL_PROMPT_BG" "$4"
}
