##################################################
# Author  : Syndamia <kamen@syndamia.com>
# Version : 
# Source  : https://gitlab.com/Syndamia/homelander
##################################################
#
# This file contains some predefined sections and variables,
# which should simplify initial configuration.
# All of them just return a colored string of characters.
#
# To use any of them, put them somewhere in your PROMPT (or RPROMPT)
# variables, with calls to the functions in homelander.zsh,
# where your text parameter is "$(function_name)".
#
# Functions that require running in the background will have defined
# "function_name_async" function, so you'll need to just add that
# to your hl_hooks() function.
#
# For more information, read README.md
#
##################################################

#
# -- Username --
#
# Username is red ($HL_USER_NOPERMS) when you don't have write permission in the current dir,
# is green ($HL_USER_SSH) when you're accessing the shell with ssh
# and is underlined when there are active background jobs.
lc_mode && HL_USER_FG=7      || HL_USER_FG=223
lc_mode && HL_USER_BG=4      || HL_USER_BG=66
lc_mode && HL_USER_NOPERMS=1 || HL_USER_NOPERMS=160
lc_mode && HL_USER_SSH=2     || HL_USER_SSH=64
HL_USER_T='%n'

hl_user() {
	local __fg=$([ ! -w . ]        && echo $HL_USER_NOPERMS || echo $HL_USER_FG)
	local __bg=$([ -n "$SSH_TTY" ] && echo $HL_USER_SSH     || echo $HL_USER_BG)
	if [ "$_jobs" -gt 0 ]; then
		local __lu='%U'
		local __ru='%u'
	fi

	echo -en $__fg $__bg "$__lu$HL_USER_T$__ru"
}

#
# -- Character before cursor (with git) --
#
# Normally it is an arrow pointing right, but changes on git status
lc_mode && HL_REPO='@'      || HL_REPO=''
lc_mode && HL_PUSH='^'      || HL_PUSH='↑'
lc_mode && HL_PULL='v'      || HL_PULL='↓'
lc_mode && HL_CHANGES='+'   || HL_CHANGES=''
lc_mode && HL_GIT_PREC='>'  || HL_GIT_PREC=''
lc_mode && HL_GIT_PREC_FG=7 || HL_GIT_PREC_FG=223
lc_mode && HL_GIT_PREC_BG=0 || HL_GIT_PREC_BG=0

hl_precursor_t() {
	cat /tmp/hl_async_hl_precursor_git_status
}

hl_precursor() {
	echo -ne $HL_GIT_PREC_FG $HL_GIT_PREC_BG "$(hl_precursor_t)"
}

hl_precursor_async() { # git status is fetched in the background
	hl_run_async hl_precursor_git_status $HL_GIT_PREC
}

hl_precursor_git_status() {
	if command -v git > /dev/null 2>&1 && git rev-parse --git-dir > /dev/null 2>&1 # if in git repo
	then
		local __gitstatus="$(git fetch >/dev/null 2>&1 && git status -uno)"
		if   echo $__gitstatus | grep -q 'behind'  > /dev/null 2>&1; then echo $HL_PULL
		elif echo $__gitstatus | grep -q 'ahead'   > /dev/null 2>&1; then echo $HL_PUSH
		elif echo $__gitstatus | grep -q 'Changes' > /dev/null 2>&1; then echo $HL_CHANGES
		else                                                              echo $HL_REPO
		fi
	else
		return 1
	fi
}

#
# -- Clock --
#
lc_mode && HL_TIME_FG=3      || HL_TIME_FG=214
lc_mode && HL_TIME_BG=0      || HL_TIME_BG=0
HL_TIME_T='%T'

hl_time() {
	echo -en $HL_TIME_FG $HL_TIME_BG "$HL_TIME_T"
}

hl_time_async() { # Time is updated every minute
	hl_run_async_cont hl_time_waiter 1
}

hl_time_waiter() {
	sleep $((60 - $(date +%s) % 60)) # Thanks https://unix.stackexchange.com/a/194656
}

#
# -- Count of background jobs --
#
lc_mode && HL_JOBS_FG=3      || HL_JOBS_FG=0
lc_mode && HL_JOBS_BG=0      || HL_JOBS_BG=140
HL_MIN_JOBS_TO_SHOW=3
HL_JOBS_T='%j'

hl_jobs_count() {
	hl_jobs_show || return 0

	echo -en $HL_JOBS_FG $HL_JOBS_BG "$HL_JOBS_T"
}
hl_jobs_show() {
	[ "$HL_MIN_JOBS_TO_SHOW" -ge 0 ] && [ "$_jobs" -ge "$HL_MIN_JOBS_TO_SHOW" ]
}

#
# -- Execution duration of last command
#
lc_mode && HL_EXEC_FG=3      || HL_EXEC_FG=111
lc_mode && HL_EXEC_BG=0      || HL_EXEC_BG=0
HL_MIN_DURATION_TO_SHOW=10

hl_exec_t() {
	echo -en ' '
	[ $_exec_dur -ge 86400 ] && echo -en "$((_exec_dur / 86400))d "
	[ $_exec_dur -ge 3600  ] && echo -en "$((_exec_dur / 3600 % 24))h "
	[ $_exec_dur -ge 60    ] && echo -en "$((_exec_dur / 60 % 60))m "
	                            echo     "$((_exec_dur % 60))s "
}

hl_exec_duration() {
	([ "$HL_MIN_DURATION_TO_SHOW" -lt 0 ] || [ "$_exec_dur" -lt "$HL_MIN_DURATION_TO_SHOW" ]) \
		&& return 0

	echo -en $HL_EXEC_FG $HL_EXEC_BG "$(hl_exec_t)"
}

#
# --Directory--
#
lc_mode && HL_PWD_FG=4 || HL_PWD_FG=66
lc_mode && HL_PWD_BG=0 || HL_PWD_BG=0
HL_PATH_T='%~'

hl_pwd() {
	echo -en $HL_PWD_FG $HL_PWD_BG "$HL_PATH_T"
}

#
# --Exit status--
#
# If exit status is zero, shows HL_OK_STATUS, otherwise HL_EXIT_T
# each having it's own colors
lc_mode && HL_EXIT_OK_FG=1 || HL_EXIT_OK_FG=76
lc_mode && HL_EXIT_FG=2    || HL_EXIT_FG=160
lc_mode && HL_EXIT_OK_BG=0 || HL_EXIT_OK_BG=0
lc_mode && HL_EXIT_BG=0    || HL_EXIT_BG=0
HL_OK_STATUS=''
HL_EXIT_T='%?'

hl_exit() {
	[ "$_exit_code" -ne 0 ] && echo -en $HL_EXIT_FG    $HL_EXIT_BG    $HL_EXIT_T    \
	                        || echo -en $HL_EXIT_OK_FG $HL_EXIT_OK_BG $HL_OK_STATUS
}

#
# --Exit code precursor--
#
# Shown as an arrow pointing right, with different colors depending on exit status
# of last program
lc_mode && HL_EXIT_PREC_FG=1     || HL_EXIT_PREC_FG=76
lc_mode && HL_EXIT_PREC_ERR_FG=2 || HL_EXIT_PREC_ERR_FG=196
lc_mode && HL_EXIT_PREC_BG=0     || HL_EXIT_PREC_BG=0
lc_mode && HL_EXIT_PRECURSOR='>' || HL_EXIT_PRECURSOR=''

hl_sim_precursor() {
	echo -en "$([ "$_exit_code" -ne 0 ] && echo $HL_EXIT_PREC_ERR_FG || echo $HL_EXIT_PREC_FG)" \
	          $HL_EXIT_PREC_BG                                                             \
	          "$HL_EXIT_PRECURSOR"
}

#
# --Linux distribution icon--
#
lc_mode && HL_DISTRO_FG=7 || HL_DISTRO_FG=223
lc_mode && HL_DISTRO_BG=0 || HL_DISTRO_BG=172

hl_distro_icon() {
	source /etc/os-release
	local __icon=''
	case $ID in
		'alpine')     __icon='' ;;
		'arch')       __icon='' ;;
		'centos')     __icon='' ;;
		'debian')     __icon='' ;;
		'elementary') __icon='' ;;
		'fedora')     __icon='' ;;
		'gentoo')     __icon='' ;;
		'mint')       __icon='' ;;
		'manjaro')    __icon='' ;;
		'opensuse')   __icon='' ;;
		'slackware')  __icon='' ;;
		'ubuntu')     __icon='' ;;
	esac
	echo -en $HL_DISTRO_FG $HL_DISTRO_BG "$__icon"
}

#
# -Battery-
#
# Shows a different character, depending on battery status
# Hoever doesn't show different colord depending on percentage
lc_mode && HL_BAT_CHARGING='+'    || HL_BAT_CHARGING=''
lc_mode && HL_BAT_DISCHARGING='-' || HL_BAT_DISCHARGING=''
lc_mode && HL_BAT_CHARGED='*'     || HL_BAT_CHARGED=''
lc_mode && HL_BAT_FG=7 || HL_BAT_FG=223
lc_mode && HL_BAT_BG=0 || HL_BAT_BG=0

  # First argument is battery indicator and second is battery percentage
hl_battery_t() {
	echo "$1 $2%%"
}

hl_battery() {
	if [ -z "$(command -v upower)" ] || \
	   [ -z "$(upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep state)" ]; then
		return 0
	fi

	local __os="$(uname)"
	if [ "${__os}" = 'Linux' ]; then
		local __bat_power=$(upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep state | awk '{print $2}')
		local __bat_ind=''
		case $__bat_power in
			'charging')      __bat_ind=$HL_BAT_CHARGING    ;;
			'discharging')   __bat_ind=$HL_BAT_DISCHARGING ;;
			'fully-charged') __bat_ind=$HL_BAT_CHARGED     ;;
		esac

		local __bat_per="$(upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep percentage | awk '{print $2}' | sed "s|%||g")"

		echo -en $HL_BAT_FG $HL_BAT_BG "$(hl_battery_t)"
	else
		echo -en $HL_PROMPT_FG $HL_PROMPT_BG 'Unsupported OS!'
	fi
}

# -Disc usage-

# -IP-

# -CPU load-

# -RAM-

# -SWAP-
